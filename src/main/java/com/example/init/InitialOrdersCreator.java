package com.example.init;

import com.example.order.Order;
import com.example.order.OrderRepository;
import com.example.person.Person;
import com.example.person.PersonRepository;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Random;
import java.util.stream.IntStream;

/**
 *
 * Inserts random Orders into the DB at app start so that you have something to query.
 *
 */
@Component
public class InitialOrdersCreator {

    public static final int ORDER_COUNT_BOUND = 99;
    public static final int PRICE_BOUND = 119;
    public static final int DATE_BOUND = 3;
    private final OrderRepository orderRepository;
    private final PersonRepository personRepository;
    private final LocalDate today = LocalDate.now();
    private final Random random = new Random();

    public InitialOrdersCreator(OrderRepository orderRepository, PersonRepository personRepository) {
        this.orderRepository = orderRepository;
        this.personRepository = personRepository;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void insertData() {
        personRepository.findAll()
                .forEach(this::addOrders);
    }

    private void addOrders(Person person) {
        int numberOfOrders = random.nextInt(ORDER_COUNT_BOUND) + 1;
        IntStream.rangeClosed(0, numberOfOrders)
                .mapToObj(i -> new Order(person.getId(), getPrice(), getDate()))
                .forEach(orderRepository::save);
    }

    private int getPrice() {
        return random.nextInt(PRICE_BOUND) + 1;
    }

    private LocalDate getDate() {
        return today.minusMonths(random.nextInt(DATE_BOUND));
    }
}
