package com.example.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("persons")
class PersonResource {

    @Autowired
    public PersonService personService;

    @GetMapping("/")
    List<Person> getAllPersons() {
        return personService.getAllPersons();
    }
}
