package com.example.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PersonService {

    private PersonRepository repository;

    @Autowired
    void setRepository(PersonRepository repo) {
        repository = repo;
    }

    public List<Person> getAllPersons() {
        List<Person> person = new ArrayList<>();
        repository.findAll().forEach(person::add);
        return person;
    }
}
