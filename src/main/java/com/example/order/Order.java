package com.example.order;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "fk_person")
    private Integer person;

    @Column
    private Integer price;

    @Column(name = "issued")
    private LocalDate date;

    public Order() {
    }

    public Order(Integer person, Integer price, LocalDate date) {
        this.person = person;
        this.price = price;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public Integer getPerson() {
        return person;
    }

    public Integer getPrice() {
        return price;
    }

    public LocalDate getDate() {
        return date;
    }
}
