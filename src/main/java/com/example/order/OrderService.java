package com.example.order;

import com.example.person.PersonService;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@Data
class OrderService {

    private final OrderRepository repository;

    private final PersonService personService;

    public Order placeNewOrder(int clientId, int price) {
        Order order = new Order(clientId, price, LocalDate.now());
        return repository.save(order);
    }

    public String fetchOrderByClientId() {
        repository.fetchCustomers();
        return "AA";
    }

}
