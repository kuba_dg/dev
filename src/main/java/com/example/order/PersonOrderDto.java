package com.example.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class PersonOrderDto {

    private Integer idCustomer;

    private Integer totalOrders;
}
