package com.example.order;

public interface ClientOrderView {
     String idCustomer();

     String totalOrders();

}
