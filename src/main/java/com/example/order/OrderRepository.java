package com.example.order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Integer> {

    @Query(value = "SELECT TOP 5  FK_PERSON AS idCustomer, COUNT(*) AS totalOrders FROM ORDERS WHERE price>50 GROUP BY  FK_PERSON Order by count(*) DESC", nativeQuery = true)
    List<ClientOrderView> fetchCustomers();

}
