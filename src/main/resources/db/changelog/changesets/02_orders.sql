--liquibase formatted sql

--changeset marek:create_order_table
create table orders (
    id int primary key auto_increment,
    fk_person int not null,
    issued date not null,
    price int not null,
    foreign key (fk_person) references person(id)
);
