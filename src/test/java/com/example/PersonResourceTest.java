package com.example;

import org.junit.jupiter.api.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class PersonResourceTest extends BaseSpringMvcTest {

    @Test
    public void responseExists() throws Exception {
        mockMvc.perform(get("/persons/")).andDo(print()).andExpect(status().isOk());
    }
}