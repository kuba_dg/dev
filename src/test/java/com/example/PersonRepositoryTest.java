package com.example;

import com.example.person.Person;
import com.example.person.PersonRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

class PersonRepositoryTest extends BaseSpringMvcTest {

    @Autowired
    PersonRepository personRepository;

    @Test
    void somePersonsExist() {
        personRepository.findAll().forEach(System.out::println);
    }

    @Test
    void regexp_works() {
        Person p1 = new Person("xxx");
        personRepository.save(p1);
        Person p2 = new Person("yyy");
        personRepository.save(p2);
        List<Person> persons = personRepository.personsWithNamesStartingWith("xx");
        if (persons.isEmpty()) {
            System.out.println("regexp doesn't work");
        }
    }
}