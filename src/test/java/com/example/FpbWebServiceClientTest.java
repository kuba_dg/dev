package com.example;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

class FpbWebServiceClientTest {

    FpbWebServiceClient fpbClient = new FpbWebServiceClient(0);

    @Test
    void fpbReportContainsTooManyIds() throws ExecutionException, InterruptedException {
        // when
        CompletableFuture<FpbWebServiceClient.FraudulentPeopleReport> future = fpbClient.downloadFraudulentPeopleReport();
        List<FpbWebServiceClient.PersonStatus> statuses = future.get().getStatuses();

        // then
        assert statuses.size() >= 10000 : "too little reports";

        // then
        int sumFraudulent = statuses.stream()
                .filter(FpbWebServiceClient.PersonStatus::isFraudulent)
                .mapToInt(s -> 1)
                .sum();
        assert sumFraudulent == 5000;
    }
}